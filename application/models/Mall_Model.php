<?php
	
Class Mall_Model extends CI_Model
{		
	public function __construct()
	{
		parent::__construct();
		$this->load->database('Mall');	
	} 

	public function login($name,$pass)
	{
		$this->db->select('*');
		$this->db->where('user_name',$name);
		$this->db->where('user_pass',$pass);
		
		$query = $this->db->get('user');
		//var_dump($query);
		//die;
		if($query->num_rows() > 0)
		{
			return $query;
		}
		else
		{
			return false;
		}
	}
	
	public function register_process($data)
	{

		$target_dir = "assets/img/";
        $target_file = $target_dir.time().basename($_FILES["image"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $imgName = time().basename($_FILES["image"]["name"]);
            move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
				
			$datas = array(

				'client_name' => $data['name'],
				'client_CNIC' => $data['cnic'],
				'client_phonenumber' => $data['number'],
				'client_address' => $data['area'], 
				'client_email' => $data['email'],
				'client_image' => $imgName
				
						);
			//print_r($datas);
			//die;

			$insert = $this->db->insert('client',$datas);
			if($insert)
			{
				$this->session->set_flashdata('success','Congratz.. You are successfully registered..!');
				redirect('Sell/user');
			}
			
			else{
				$this->session->set_flashdata('success','Your Email already exits please go for login ');
				redirect('Sell/user');
			}
		}

		public function insertpropert($data)
		{
			
			$datas = array(

				'Categories' => $data['Categories'], 
				'Outlet_Number' => $data['Outlet_Number'],
				'property_floor' => $data['Property_Floor'],
				'Area_SQ_FT' => $data['Area_SQ_FT'],
				'RENT_PER_SQ_FT' => $data['RENT_PER_SQ_FT'],
				'total_rent' => $data['total_rent'], 
				'property_discription' => $data['property_description']
				


			);
			$query = $this->db->insert('property',$datas);
		

		}

		public function insertclient($data)
		{
			$target_dir = "assets/document/";
        $target_file = $target_dir.time().basename($_FILES["imgName"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $imgNamee = time().basename($_FILES["imgName"]["name"]);
            move_uploaded_file($_FILES["imgName"]["tmp_name"], $target_file);

			$datas = array(

				'client_name' => $data['client_name'] ,
				'client_CNIC' => $data['client_cnic'],
				'client_email' => $data['email'], 
				'client_phonenumber' => $data['phone'] ,
				'client_address' => $data['address'],
				'shop_id' => $data['property_id'],
				'client_image' => $imgNamee

			);
			$query = $this->db->insert('client',$datas);
		

		}

public function InsertDataClient($data)
		{
			

			$datas = array(

				
				
				'client_CNIC' => $data['cnic'],
				'client_email' => $data['email'], 
				'representative_name' => $data['representive_name'],
				'company_name' => $data['company_name'],
				'ntn' => $data['ntn'],
				'address' => $data['address'],
				'client_phonenumber' => $data['contact'],
				'company_contact' => $data['company_contact']

				
			);
			$query = $this->db->insert('client',$datas);
		

		}


		//edit property
		public function propertyedit($data,$id)
		{
			
			$datas = array(

				'Categories' => $data['Categories'], 
				'Outlet_Number' => $data['Outlet_Number'] ,
				'property_floor' => $data['property_floor'] ,
				'AREA_SQ_FT' => $data['Area_SQ_FT'],
				'RENT_PER_SQ_FT' => $data['RENT_PER_SQ_FT'],
				'property_discription' => $data['property_description'],


			);
			$datas = array(

				'Categories' => $data['Categories'], 
				'Outlet_Number' => $data['Outlet_Number'] ,
				'property_floor' => $data['property_floor'] ,
				'AREA_SQ_FT' => $data['Area_SQ_FT'],
				'RENT_PER_SQ_FT' => $data['RENT_PER_SQ_FT'],
				'property_discription' => $data['property_description'],


			);
			$this->db->where('property_id',$id);
			$query = $this->db->update('property',$datas);
		

		}


public function EditBookingFromPropertyEditing($data,$id)
{

		$datas = array(

				'shop_title' => $data['title'], 
				'shop_no' => $data['pno'] 

			);
			$this->db->where('property_id',$id);
			$query = $this->db->update('booking',$datas);
}



public function editclient($data,$id) 
{
		$target_dir = "assets/document/";
        $target_file = $target_dir.time().basename($_FILES["imgName"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $imgNamee = time().basename($_FILES["imgName"]["name"]);
        move_uploaded_file($_FILES["imgName"]["tmp_name"], $target_file);
 		
 		$datas = array(
			
						'company_name' => $data['company_name'],
						'ntn' => $data['ntn'],
						'address' => $data['address'],
						'company_contact' => $data['company_contact'],
						'representative_name' => $data['representative_name'],
						'client_CNIC' => $data['cnic'], 
						'client_phonenumber' => $data['phone'],						
						'client_email' => $data['email'],
						'client_image' => $imgNamee 

					 );
			$this->db->where('client_id',$id);
			$query = $this->db->update('client',$datas);
			return $query;

}


public function  ShowPropertyOfClientFromBooking($id)
{
	$query= $this->db->query("select * from booking where client_id='".$id."'");
			return $query;



}


public function  ShowPropertyOfClientFromProperty($id)
{
	$query= $this->db->query("select * from property where property_id='".$id."'");
			return $query;



}

		public function showpropertyedit($id){
			/*$this->db->where('property_id',$id);

			$this->db->select('*');
			$query1= $this->db->get('property');*/
			$query= $this->db->query("select * from property where property_id='".$id."'");
			return $query;


		}

		public function getDataForEditClient($id){
			
			$query= $this->db->query("select * from client where client_id='".$id."'");
			return $query;


		}



		public function all_property_booked()
		{
			$query = $this->db->query('select booking.*
				,brand.brand_name
				,property.*  
				from booking
				INNER JOIN brand
				ON booking.brand_id = brand.brand_id 
				INNER JOIN property 
				ON booking.property_id = property.property_id');
			return $query;
		}
			public function all_property_available()
		{
			$query = $this->db->query('select * from property where property_book=0');
			return $query;
		}

		public function all_clients()
		{
			$query = $this->db->query('select * from client');
			return $query;
		}
		//bookingedit here
	public function bookingedit($data,$id)
		{	
				$target_dir = "../assets/document/";
        $target_file = $target_dir.time().basename($_FILES["imgName"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $imgName = time().basename($_FILES["imgName"]["name"]);
        
        move_uploaded_file($_FILES["imgName"]["tmp_name"], $target_file);

            
            $RENT_PER_SQ_FT = $data['rent_SQ_FT'];
  			$Area_SQ_FT = $data['AREA_SQ_FT'];
    		$total_rent = $RENT_PER_SQ_FT * $Area_SQ_FT;

			$datas = array(

				
				'bookingdate' => $data['bookdate'], 
				'bookingprice' => $data['bookingprice'] ,			
				'description' => $data['pdesc'],
				'rent_date' => $data['rent_date'], 
				'rent_month' => $data['rent_month'] ,			
				'rent_year' => $data['rent_year'],
				'RENT_PER_SQ_FT' => $data['rent_SQ_FT'],				
				'AREA_SQ_FT' => $data['AREA_SQ_FT'],
				'total_rent' => $total_rent,				
				'upload' => $imgName

			);
			$this->db->where('book_id',$id);
			$query = $this->db->update('booking',$datas);

		}



			public function show_book_booking($id)
			{
				

				$query = $this->db->query("select * from booking where property_id= $id");
				return $query;
			}
			public function show_property_booking($id)
			{
			

				$query = $this->db->query("select Categories,Outlet_Number,property_floor from property where property_id= $id");
				return $query;
			}
			public function GetDataForInsertBooking($property_id)

{
		$query=$this->db->query("select * from property where property_id= $property_id ");
	/*$this->db->where('property_id',$property_id);
	$this->db->select('*');

	$query = $this->db->get('property');*/
	return $query;
}
////////////////////////////insert booking here
		public function insertbooking($data)
		{	
				$target_dir = "assets/document/";
        $target_file = $target_dir.time().basename($_FILES["imgName"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $imgName = time().basename($_FILES["imgName"]["name"]);
        
        move_uploaded_file($_FILES["imgName"]["tmp_name"], $target_file);
        

            $client_rental_per_sq_ft = $data['rent_SQ_FT'];
  			$Area_SQ_FT = $data['AREA_SQ_FT'];
    		$total_rent = $client_rental_per_sq_ft * $Area_SQ_FT;
         	$datas = array(

				'brand_id' => $data['brand_id'],
				'property_id' => $data['property_id'],
				'bookingdate' => $data['bookdate'], 
				'bookingprice' => $data['bookingprice'] ,			
				'description' => $data['pdesc'],
				'rent_date' => $data['rent_date'], 
				'rent_month' => $data['rent_month'] ,			
				'rent_year' => $data['rent_year'],
				'RENT_PER_SQ_FT' => $data['rent_SQ_FT'],				
				'AREA_SQ_FT' => $data['AREA_SQ_FT'],
				'total_rent' => $total_rent,
				
				'upload' => $imgName

			);
			$query = $this->db->insert('booking',$datas);	
		}
		public function insertbookingstatus($data)
		{	

			$datas = array(
				'property_book'=>'1'
			);
			$this->db->where('property_id',$data['property_id']);
			$query = $this->db->update('property',$datas);	
		

		}
			public function brand()
		{	

		
			$query = $this->db->query('Select * from brand');
			return $query->result();	
		

		}


		////insert into booking end there
		//lead
		public function showlead()
		{
				$query = $this->db->query('select * from lead');
				return $query;


		}
		public function lead_insert($data)
		{
			$datas=array(
				'lead_name'=> $data['lead_name'],
				'lead_phone'=> $data['lead_phone'],
				'lead_email'=> $data['lead_email'],
				'description'=> $data['lead_description']
			);
			$this->db->insert('lead',$datas);
		}
		public function show_lead_edit($id){

			$query =$this->db->query("select * from lead where lead_id = '".$id. "'");
			return $query;
		}



		public function lead_edit($data,$id)
		
		{
			$datas=array(
				'lead_name'=> $data['lead_name'],
				'lead_phone'=> $data['lead_phone'],
				'lead_email'=> $data['lead_email'],
				'description'=> $data['lead_description']
			);
			$this->db->where('lead_id',$id);
			$this->db->update('lead',$datas);
		}




/*public function EditClientFromBooking($data,$id)


{


$datas = array(

				'client_name' => $data['bookdate'], 
				'client_CNIC' => $data['bookingprice'] ,
				'client_phonenumber' => $data['client_name'] ,
				'client_address' => $data['client_cnic'],
				'client_email' => $data['pdesc']

			);
			$this->db->where('shop_id',$id);
			$query = $this->db->update('client',$datas);


}*/







//category

public function categories_insert($data)
		{
			$datas=array(
			 'category'=> $data['category_name']
			);
			$this->db->insert('categories',$datas);
		}


		public function show_category_edit($id)
		{

			$query =$this->db->query("select * from categories where category_id = '".$id. "'");
			return $query;
		}


		public function category_edit($data,$id)
		
		{
			$datas=array(
				'category'=> $data['name'],
			);
			$this->db->where('category_id',$id);
			$this->db->update('categories',$datas);
		}

		public function all_category()
		{

			$query =$this->db->query("select * from categories");
			return $query;
		}



//gernel
		public function categories()
		{
			$query = $this->db->query('select * from categories');
			return $query;
		}


	//brand

		public function all_brand()
		{

			$query =$this->db->query("select * from brand");
			return $query;
		}


		public function brands_insert($data)
		{
			$datas=array(
			 'brand_name'=> $data['brand_names'],	
			 'company_name' => $data['company_names']
			);
			$this->db->insert('brand',$datas);
		}

		public function show_brands_edit($id)
		{

			$query =$this->db->query("select * from brand where brand_id = '".$id. "'");
			return $query;
		}


		public function brands_edit($data,$id)
		
		{
			$datas=array(
			 'brand_name'=> $data['brand_names'],	
			 'company_name' => $data['company_names']
			);
			$this->db->where('brand_id',$id);
			$this->db->update('brand',$datas);
		}


	public function all_company()
	{
		$query= $this->db->query('SELECT company_name FROM client');
		return $query;
	}
}

?>