
<?php $this->load->view('Admin/include/header'); ?>
<?php $this->load->view('Admin/include/header_top'); ?>

<main class="cd-main-content">
  <?php $this->load->view('Admin/include/nav'); ?>
        

        <div class="content-wrapper">
           <form action="/action_page.php">
  <div class="container">
    <h1>Register</h1>
    <p>Please fill in this form to create an account.</p>
    <hr>
    
    <label for="name"><b>Name</b></label>
    <input type="text" placeholder="Enter Name" name="name" required>
    
    <label for="Cnic"><b>CNIC</b></label>
    <input type="text" placeholder="Enter CNIC" name="cnic" required>
    
    <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Enter Email" name="email" required>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" required>

    <label for="psw-repeat"><b>Repeat Password</b></label>
    <input type="password" placeholder="Repeat Password" name="psw-repeat" required>
    <hr>
    <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>

    <button type="submit" class="registerbtn">Register</button>
  </div>
  
</form>
        </div> <!-- .content-wrapper -->
    </main> <!-- .cd-main-content -->

<?php $this->load->view('Admin/include/footer'); ?>