<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sell extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$method = $this->router->fetch_method();
		$methods = array('index');

		if(in_array($method,$methods))
		{
			if(!$this->session->userdata('user_name'))
			{
				redirect(base_url('Sell/login_form'));
			}
		}
	}




	public function index()
	{
		if($this->session->userdata('user_name')!=null)
		{
			redirect(base_url('Sell/all_booking'));
		}
		else
		{
			redirect(base_url('Sell/login_form'));
		}
	}


	public function login_form()
	{

		$this->load->view('index');
	}


	public function login()
	{
		$this->load->model('Mall_Model');
		$name = $this->input->post('name');
		$pass = $this->input->post('psw');
		
		$result = $this->Mall_Model->login($name,$pass);
		
		//print_r($result);
		//die;
		
		if($result == TRUE)
		{
			$this->session->set_userdata('user_name');
			$session = array(

				'user_name' => TRUE

					);
			$this->session->set_userdata($session);
			$this->session->set_flashdata('success','Your are logged in...');
			redirect(base_url('Sell/index'));
		}
		else
		{
			$this->session->set_flashdata('error','Invalid Name or Password');
			redirect(base_url('Sell/login_form'));	
		}
	}



	public function logout()
	{
		$this->session->sess_destroy();
		$this->session->set_flashdata('error','Successfully Logout.');
		redirect(base_url());
	}

	public function user()
	{
		if($this->session->userdata('user_name')!=null)
		{
		$this->load->view('Admin/user');
	}
	else
		{
			redirect(base_url('Sell/login_form'));
		}
	}


	//property start


	public function all_property()
	{
	if($this->session->userdata('user_name')!=null)
		{
		$this->load->model('Mall_Model');
		$query['property'] = $this->Mall_Model->all_property_booked();
		$query['available'] = $this->Mall_Model->all_property_available();

		$this->load->view('Admin/show_property',$query);
			}
	else
		{
			redirect(base_url('Sell/login_form'));
		}
	}

	public function property()			//insertpropertyshow
	{
		if($this->session->userdata('user_name')!=null)
		{
			$this->load->model('Mall_Model');
			$query['categories'] = $this->Mall_Model->categories();
			$this->load->view('Admin/add_property',$query);
	}
	else
		{
			redirect(base_url('Sell/login_form'));
		}
	}


	
	public function propertyinsert()
	{
	if($this->session->userdata('user_name')!=null)
		{
		$data = $this->input->post();
		$this->load->model('Mall_Model');
		$query = $this->Mall_Model->insertpropert($data);

		redirect('Sell/all_property');
	}
	else
		{
			redirect(base_url('Sell/login_form'));
		}
	}


	
	public function property_delete($id)
	{
		if($this->session->userdata('user_name')!=null)
		{
		$this->db->where('property_id',$id);
		$this->db->delete('property');
		redirect('sell/all_property');
		}

	}

	

		public function showeditproperty($id)
	{
		if($this->session->userdata('user_name')!=null)
		{
			$this->load->model('Mall_Model');
		$query['booking'] = $this->Mall_Model->showpropertyedit($id);
		
		$this->load->view('Admin/edit_property',$query);
					}
		else
		{
			redirect(base_url('Sell/login_form'));
		}

	}


	public function propertyedit($id)
	{
		if($this->session->userdata('user_name')!=null)
		{
		$data = $this->input->post();
		$this->load->model('Mall_Model');
		$query = $this->Mall_Model->propertyedit($data,$id);
		$query = $this->Mall_Model->EditBookingFromPropertyEditing($data,$id);
		redirect('sell/all_property');
					}
		else
		{
			redirect(base_url('Sell/login_form'));
		}
	}
		public function ShowPropertyOfClient($id){

		if($this->session->userdata('user_name')!=null)
		{
		$this->load->model('Mall_Model');
		$query['ShowPropertyOfClientFromBooking'] = $this->Mall_Model->ShowPropertyOfClientFromBooking($id);
		$query['ShowPropertyOfClientFromBooking2'] = $this->Mall_Model->ShowPropertyOfClientFromBooking($id);
		$query['ShowPropertyOfClientFromProperty'] = $this->Mall_Model->ShowPropertyOfClientFromProperty($id);
		$this->load->view('Admin/ShowPropertyOfClient',$query); 
					}
		else
		{
			redirect(base_url('Sell/login_form'));
		}

}
			//property end


		//client

		public function all_clients()
		{
		if($this->session->userdata('user_name')!=null)
		{
		$this->load->model('Mall_Model');
		$query['clients'] = $this->Mall_Model->all_clients();
		$this->load->view('Admin/show_client',$query);
					}
	else
		{
			redirect(base_url('Sell/login_form'));
		}
	}

	public function ShowInsertClient()
	{

	if($this->session->userdata('user_name')!=null)
		{	
	
		$this->load->view('Admin/add_client');
					}
	else
		{
			redirect(base_url('Sell/login_form'));
		}

	}


	public function register_Client()
	{
		if($this->session->userdata('user_name')!=null)
		{
		$this->load->model('Mall_Model');
		$data = $this->input->post();
		$this->Mall_Model->register_process($data);
		}
		else
		{
			redirect(base_url('Sell/login_form'));
		}

	}

	public function InsertDataClient()

	{
	if($this->session->userdata('user_name')!=null)
		{
	$data = $this->input->post();
		$this->load->model('Mall_Model');

		$query = $this->Mall_Model->InsertDataClient($data);

		redirect("Sell/all_clients");
		
					}
	else
		{
			redirect(base_url('Sell/login_form'));
		}
}
	

public function Edit_Client($id)

{
	if($this->session->userdata('user_name')!=null)
		{
        $data = $this->input->post();
		$this->load->model('Mall_Model');
		$query = $this->Mall_Model->editclient($data,$id);
		redirect('Sell/all_clients');
		}
	else
		{
			redirect(base_url('Sell/login_form'));
		}


}

public function showEditClient(){

		if($this->session->userdata('user_name')!=null)
		{

		   $this->load->view('Admin/edit_client');
						}
		else
		{
			redirect(base_url('Sell/login_form'));
		}



}

	public function getDataForeditclient($id)
	{
		if($this->session->userdata('user_name')!=null)
		{
		$this->load->model('Mall_Model');
		$query['EditClient'] = $this->Mall_Model->getDataForEditClient($id);
		$this->load->view('Admin/edit_client',$query); 
		}
		else
		{
			redirect(base_url('Sell/login_form'));
		}

		
	}
				// client end



	
	// booking start   

	public function all_booking()
	{
		if($this->session->userdata('user_name')!=null)
		{
		$this->load->model('Mall_Model');
		$query['property'] = $this->Mall_Model->all_property_booked();
		$query['available'] = $this->Mall_Model->all_property_available();
		$query['categories'] = $this->Mall_Model->categories();

		$this->load->view('Admin/show_booking',$query);
	}
	
	}

	public function booking($id)
	{
		if($this->session->userdata('user_name')!=null)
		{


          $this->load->model('Mall_Model');
		  $query['property'] = $this->Mall_Model->GetDataForInsertBooking($id);
		  $query['company_name'] = $this->Mall_Model->brand();
		  $this->load->view('Admin/add_booking',$query);

		 }
		else
		{
			redirect(base_url('Sell/login_form'));
		}

	}


	public function bookinginsert()
	{
		if($this->session->userdata('user_name')!=null)
		{
		$data = $this->input->post();
		$this->load->model('Mall_Model');

		$query = $this->Mall_Model->insertbooking($data);
		$query1 = $this->Mall_Model->insertbookingstatus($data);
		
	

		redirect('sell/all_booking');
					}
		else
		{
			redirect(base_url('Sell/login_form'));
		}
	}

	public function showbookingedit($id)
	{
		if($this->session->userdata('user_name')!=null)
		{
		$this->load->model('Mall_Model');
		
		$query['property'] = $this->Mall_Model->show_property_booking($id);

		$query['booking'] = $this->Mall_Model->show_book_booking($id);
		$this->load->view('Admin/edit_booking',$query); 
					}
		else
		{
			redirect(base_url('Sell/login_form'));
		}
		
	}



	public function bookingedit($id)
	{
		if($this->session->userdata('user_name')!=null)
		{
		$data = $this->input->post();	
		$this->load->model('Mall_Model');
		$query = $this->Mall_Model->bookingedit($data,$id);
		redirect('sell/all_booking');
	}
		else
		{
			redirect(base_url('Sell/login_form'));
		}

	}



	public function viewbooking($id)
	{
		if($this->session->userdata('user_name')!=null)
		{
		$this->load->model('Mall_Model');
		
		$query['property'] = $this->Mall_Model->show_property_booking($id);

		$query['booking'] = $this->Mall_Model->show_book_booking($id);
		$this->load->view('Admin/view_booking',$query); 
	}
		else
		{
			redirect(base_url('Sell/login_form'));
		}
		
	}


		

	// booking end there

	


	//lead 

	public function all_lead()
	{
		if($this->session->userdata('user_name')!=null)
		{
			$this->load->model('Mall_Model');
			$query['lead']= $this->Mall_Model->showlead();     
			$this->load->view('Admin/show_lead',$query);		 
		}
	}


	 public function lead_delete($id)
	 {
	 	if($this->session->userdata('user_name')!=null)
		{		
			$this->db->where('lead_id',$id);
			$query = $this->db->delete('lead');
			redirect('sell/all_lead');
		}



	 }
 	 public function show_lead_insert()
	 {
	 	if($this->session->userdata('user_name')!=null)
		{			
			$this->load->view('Admin/add_lead');

		 
		}
	}
	public function lead_insert()
	 {
	 	if($this->session->userdata('user_name')!=null)
		{	$data= $this->input->post();
			$this->load->model('Mall_Model');
			$query = $this->Mall_Model->lead_insert($data);		
			$this->load->view('Admin/add_lead');
			redirect('sell/all_lead');
				}
		else
		{
			redirect(base_url('Sell/login_form'));
		}
	}
	
	
		public function show_lead_edit($id)
	 {
	 	if($this->session->userdata('user_name')!=null)
		{	
			$this->load->model('Mall_Model');
			$query['edit_lead']= $this->Mall_Model->show_lead_edit($id);
			$this->load->view('Admin/edit_lead',$query);

		 
						}
		else
		{
			redirect(base_url('Sell/login_form'));
		}
	}

		public function lead_edit($id)
	 {
	 	if($this->session->userdata('user_name')!=null)
		{	$data= $this->input->post();
			$this->load->model('Mall_Model');
			$query = $this->Mall_Model->lead_edit($data,$id);		
			
			redirect('sell/all_lead');

		 
		}
	}











	//Category
	public function show_category_list()
	{
		if($this->session->userdata('user_name')!=null)
		{
		$this->load->model('Mall_Model');
		$query['all_category']=$this->Mall_Model->all_category();	
		$this->load->view('Admin/show_category',$query);
		}
		else
		{
			redirect(base_url('Sell/login_form'));
		}

	}

	public function show_category_insert()
	{
		if($this->session->userdata('user_name')!=null)
		{	
		$this->load->view('Admin/add_category');
						}
		else
		{
			redirect(base_url('Sell/login_form'));
		}

	}

	public function category_insert()
	{
		if($this->session->userdata('user_name')!=null)
		{	
			$data= $this->input->post();
			$this->load->model('Mall_Model');
			$query = $this->Mall_Model->categories_insert($data);		
			redirect('sell/show_category_list');
						}
		else
		{
			redirect(base_url('Sell/login_form'));
		}

	}


		public function show_category_edit($id)
	 {
	 	if($this->session->userdata('user_name')!=null)
		{
			$this->load->model('Mall_Model');
			$query['editcat'] = $this->Mall_Model->show_category_edit($id);		
			$this->load->view('Admin/edit_category',$query);
				}
		else
		{
			redirect(base_url('Sell/login_form'));
		}
	}

	 public function category_delete($id)
	 {
	 	if($this->session->userdata('user_name')!=null)
		{	
			$this->db->where('category_id',$id);
			$query = $this->db->delete('categories');
			redirect('sell/show_category_list');
				}
		else
		{
			redirect(base_url('Sell/login_form'));
		}

	 }

		public function ctg_edit($id)
	 {
	 	if($this->session->userdata('user_name')!=null)
		{	$data= $this->input->post();
			$this->load->model('Mall_Model');
			$query = $this->Mall_Model->category_edit($data,$id);		
			
			redirect('sell/show_category_list');
		 
		}

}








	//brand

	public function show_brand()
	{
		if($this->session->userdata('user_name')!=null)
		{
		$this->load->model('Mall_Model');
		$query['brands'] = $this->Mall_Model->all_brand();	
		$this->load->view('Admin/show_brand',$query);
				}
		else
		{
			redirect(base_url('Sell/login_form'));
		}

}
	public function show_brand_insert()
	{
		if($this->session->userdata('user_name')!=null)
		{	
		
		$this->load->model('Mall_Model');
		$query['all_brand'] = $this->Mall_Model->all_company();
		$this->load->view('Admin/add_brand',$query);
				}
		else
		{
			redirect(base_url('Sell/login_form'));
		}

	}

	public function brand_insert()
	{
		if($this->session->userdata('user_name')!=null)
		{	
			$data= $this->input->post();
			$this->load->model('Mall_Model');
			$query = $this->Mall_Model->brands_insert($data);		
			redirect('Sell/show_brand');
		}
		else
		{
			redirect(base_url('Sell/login_form'));
		}
	}


	public function brand_delete($id)
	 {
	 	if($this->session->userdata('user_name')!=null)
		{	
			$this->db->where('brand_id',$id);
			$query = $this->db->delete('brand');
			redirect('sell/show_brand');
				}
		else
		{
			redirect(base_url('Sell/login_form'));
		}

	 }


	public function show_brand_edit($id)
	 {
	 	if($this->session->userdata('user_name')!=null)
		{
			$this->load->model('Mall_Model');
			$query['editbrnd'] = $this->Mall_Model->show_brands_edit($id);	
			$query['all_brand'] = $this->Mall_Model->all_company();
			$this->load->view('Admin/edit_brand',$query);
				}
		else
		{
			redirect(base_url('Sell/login_form'));
		}
	}	


		public function brand_edit($id)
	 {
	 	if($this->session->userdata('user_name')!=null)
		{	$data= $this->input->post();
			$this->load->model('Mall_Model');
			$query = $this->Mall_Model->brands_edit($data,$id);		
			
			redirect('Sell/show_brand');
		 
				}
		else
		{
			redirect(base_url('Sell/login_form'));
		}
	}
	//end brand







	public function ajax()
	 {
	 
			$this->load->view('Admin/ajax');		 
		
	}
		public function ajax1()
	 {

			$this->load->view('Admin/ajax1'); 
	
	}
		
public function ajax2()
	 {
	 
			$this->load->view('Admin/ajax2');
		
	 }







}
