<nav class="navbar p-l-5 p-r-5">
    <ul class="nav navbar-nav navbar-left">
            <li>
                <div class="navbar-header" style="margin-top: 5%;">
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand" href="index.html"><img src="<?php echo base_url();?>assets/images/orglogo.png" width="42%" alt="Falcon"></a>
                </div>
            </li>
            
            
                <ul class="dropdown-menu pullDown">
                        <li class="header"></li>
                        <li class="body"></li>
                </ul>
            
            <li class="float-right">

                    <a href="javascript:void(0);" class="fullscreen hidden-sm-down" data-provide="fullscreen" data-close="true">
                        <i class="zmdi zmdi-fullscreen"></i>
                    </a>

                    <a href="<?php echo base_url('Sell/logout'); ?>" class="mega-menu" data-close="true">
                        <i class="zmdi zmdi-power"></i>
                    </a>
              
                    <a href="javascript:void(0);" class="js-right-sidebar" data-close="true">
                        <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
                    </a>
            </li>
    </ul>
</nav>
