<aside id="leftsidebar" class="sidebar">
 
    <div class="tab-content">
         <div class="menu">
            <ul class="list">
              <li>
                <div class="user-info">
                    <div class="image">
                        <a href="profile.html"><img src="<?php echo base_url();?>assets/images/profile_av.png" alt="User"></a>
                    </div>
                </div>
              </li>

        <li class="header">
            <h5><strong>MAIN</strong></h5>
        </li>
                    <li> 
                           <a href="javascript:void(0);"><i class="zmdi zmdi-time"></i><span><strong>Overview</strong></span></a>
                    </li>
                  
                    <li> 
                        <a href="<?php echo base_url('Sell/all_booking'); ?>"><i class="zmdi zmdi-apps"></i>
                        <span><strong>Booking</strong></span></a>
                    </li>

                    <li> 
                        <a href="<?php echo base_url('Sell/all_property'); ?>" ><i class="zmdi zmdi-shopping-cart"></i>
                        <span><strong>Property</strong></span></a>
                    </li>

                    <li> 
                        <a href="<?php echo base_url('sell/all_lead'); ?>" ><i class="zmdi zmdi-swap-alt"></i>
                        <span><strong>Lead</strong></span></a>
                    </li>

                    <li> 
                        <a href="<?php echo base_url('Sell/all_clients'); ?>" ><i class="zmdi zmdi-assignment"></i>
                        <span><strong>Clients</strong></span></a> 
                    </li>

                    <li> 
                        <a href="<?php echo base_url('Sell/show_category_list'); ?>" ><i class="zmdi zmdi-grid"></i>
                        <span><strong>Category</strong></span></a>
                    </li>            

                    <li>
                        <a href="<?php echo base_url('Sell/show_brand'); ?>" ><i class="zmdi zmdi-chart"></i>
                        <span><strong>Brand</strong></span></a>
                    </li>
               
            </li>
        </ul>
        </div>
       
    </div>    
</aside>