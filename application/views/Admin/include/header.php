<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/mystyle.css">

	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css"> <!-- Resource style -->
	<script src="<?php echo base_url(); ?>assets/js/modernizr.js"></script> <!-- Modernizr -->
  	
	<title>Shopping Mall</title>
</head>
<body style="background-image: url(<?php echo base_url(); ?>"assets/img/back-img.jpg);">
