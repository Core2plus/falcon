<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>:: Falcon Mall ::</title>
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Favicon-->
<link  rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
<!-- Morris Chart Css-->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/morrisjs/morris.css" />
<!-- Colorpicker Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
<!-- Multi Select Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/multi-select/css/multi-select.css">
<!-- Bootstrap Spinner Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/jquery-spinner/css/bootstrap-spinner.css">
<!-- Bootstrap Tagsinput Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<!-- Bootstrap Select Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.css" />
<!-- noUISlider Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/nouislider/nouislider.min.css" />
<!-- Custom Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/color_skins.css">
</head>
<body class="theme-blue">

<!--header-->
<?php $this->load->view('Admin/include/header_top'); ?>

<!-- Top Bar -->
<?php $this->load->view('Admin/include/header_top'); ?>

<!-- Left Sidebar -->
<?php $this->load->view('Admin/include/nav'); ?>


<!-- Right Sidebar -->
<?php $this->load->view('Admin/include/nav1'); ?>


<br>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2><strong>Welcome To FALCON</strong></h2>
                </div>
    
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Sell/index'); ?>"><i class="zmdi zmdi-home"></i> Falcon</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Sell/all_clients'); ?>"><i class="zmdi zmdi-assignment"></i> Client</a></li>
                    <li class="breadcrumb-item active">Edit Client</li>
                </ul>
            </div>        

            </div>
        </div>
    </div>
  
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">

                        <h2><strong>EDIT CLIENT</strong> </h2>
                        <?php
        
        foreach ($EditClient->result_array() as $EditClient ) {
          # code...
        
      ?>
           <form action="<?php echo base_url('Sell/Edit_Client/').$EditClient['client_id'] ?>" method="post" enctype="multipart/form-data">
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label><strong>Enter Company</strong></label>
                                    <input type="text" class="form-control" placeholder="Enter Company Name" value="<?php echo $EditClient['company_name'];?>" name="company_name">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><strong>NTN</strong></label>
                                    <input type="text" class="form-control" placeholder="Enter NTN" value="<?php echo $EditClient['ntn'];?>" name="ntn">
                                </div>
                        </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label><strong>Enter Address</strong></label>
                                    <input type="text" class="form-control" placeholder="Address" value="<?php echo $EditClient['address'];?>" name="address">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><strong>Contact</strong></label>
                                    <input type="int" class="form-control" placeholder="Enter Number" value="<?php echo $EditClient['company_contact'];?>" name="company_contact">
                                </div>
                            </div>
                        
                        
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label><strong>Representive Name</strong></label>
                                    <input type="text" class="form-control" placeholder="Enter Representive Name" value="<?php echo $EditClient['representative_name'];?>" name="representative_name">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><strong>CNIC</strong></label>
                                    <input type="text" class="form-control" placeholder="Enter CNIC" value="<?php echo $EditClient['client_CNIC'];?>" name="cnic">
                                </div>
                        </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><strong>Contact Number</strong></label>
                                    <input type="text" class="form-control" placeholder="Enter Contact Number" value="<?php echo $EditClient['client_phonenumber'];?>" name="phone">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><strong>Email</strong></label>
                                    <input type="text" class="form-control" placeholder="Enter Email Address" value="<?php echo $EditClient['client_email'];?>" name="email">
                                </div>
                            </div>
  
                </div>
                 

                 <div class="row">
                    <div class="file btn btn-lg btn-primary" style="  position: relative;
                        overflow: hidden; font-size:15px; border-radius: 10px"> <i class="fa fa-folder"></i>              
                            Upload
                        <input type="file" style=" position: absolute;  font-size: 15px;  opacity: 0;
                         right: 0;  top: 0;" name="imgName"/>
                    </div>
                </div>
<?php }?>
                        <div class="col-md-12" >
                           <button type="submit" class="btn btn-success" style="float: right;border-radius: 25px;">Update</button> 
                        </div>
                    
                </div>
                </div>
            </div>
                   
            </div>
        </form>

        </div>
        <!-- #END# Multi Column --> 
    </div>
</section>
<!-- Jquery Core Js --> 
<script src="<?php echo base_url();?>assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="<?php echo base_url();?>assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

<script src="<?php echo base_url();?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> <!-- Bootstrap Colorpicker Js --> 
<script src="<?php echo base_url();?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> <!-- Input Mask Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/multi-select/js/jquery.multi-select.js"></script> <!-- Multi Select Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/jquery-spinner/js/jquery.spinner.js"></script> <!-- Jquery Spinner Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script> <!-- Bootstrap Tags Input Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/nouislider/nouislider.js"></script> <!-- noUISlider Plugin Js --> 

<script src="<?php echo base_url();?>assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
<script src="<?php echo base_url();?>assets/js/pages/forms/advanced-form-elements.js"></script> 
</body>
<?php $this->load->view('Admin/include/footer'); ?>
</html>