<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>:: Falcon Mall ::</title>
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Favicon-->
<link  rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
<!-- Morris Chart Css-->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/morrisjs/morris.css" />
<!-- Colorpicker Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
<!-- Multi Select Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/multi-select/css/multi-select.css">
<!-- Bootstrap Spinner Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/jquery-spinner/css/bootstrap-spinner.css">
<!-- Bootstrap Tagsinput Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<!-- Bootstrap Select Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.css" />
<!-- noUISlider Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/nouislider/nouislider.min.css" />
<!-- Custom Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/color_skins.css">
</head>
<body class="theme-blue">

<!--header-->
<?php $this->load->view('Admin/include/header_top'); ?>

<!-- Top Bar -->
<?php $this->load->view('Admin/include/header_top'); ?>

<!-- Left Sidebar -->
<?php $this->load->view('Admin/include/nav'); ?>


<!-- Right Sidebar -->
<?php $this->load->view('Admin/include/nav1'); ?>


<br>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2><strong>Welcome To FALCON</strong></h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Sell/index'); ?>"><i class="zmdi zmdi-home"></i> Falcon</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Sell/all_property'); ?>"><i class="zmdi zmdi-grid"></i> Property</a></li>
                    <li class="breadcrumb-item active">Edit Property</li>
                </ul>
            </div>        

            </div>
        </div>
    </div>
  
        <!-- Advanced Select -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Edit PROPERTY</strong> </h2>
                        <?php  foreach ($booking->result_array() as $key) {
       # code...
    ?>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-md-4">
                                <div class="col-md-12">
                                <div class="form-group">
                                    <label style="margin-left: 20px;"><strong>Categories</strong></label>
                                    <input type="text" class="form-control" placeholder="Property Name" value="<?php echo $key['Categories'];?>" name="Categories"/>
                                </div>
                            </div>
                            </div>
                            <div class=" col-md-8" ;">
                                <div class="form-group">
                                <label style="margin-left: 20px;"><strong>Outlet Number</strong></label>
                                <input type="text" class="form-control" value = "<?php echo $key['Outlet_Number']?>" name="Outlet_Number" /> 
                                </div>
                        </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label style="margin-left:25px;"><b>Floor Selection</b></label>
                                    <select name="property_floor" class="form-control" style="border-radius:20px;">
                                        <option  value="<?php echo $key['property_floor'];?>"> <?php echo $key['property_floor'];?> </option>
                                        <option value="Ground Floor">Ground Floor</option>
                                        <option value="1ST Floor">1ST Floor</option>
                                        <option value="2ND Floor">2ND Floor</option>
                                        <option value="3RD Floor">3RD Floor</option>
                                    </select >
                                    
                                                               
                                <div class="col-md-12">
                                <div class="form-group">
                                    <label style="margin-left: 20px;"><strong>Area Sq Ft</strong></label>
                                    <input type="text" class="form-control" name="Area_SQ_FT" value=" <?php echo $key['AREA_SQ_FT']; ?>" >
                                </div>
                                 <div class="form-group">
                                    <label style="margin-left: 20px;"><strong>Rent Sq Ft</strong></label><input type="text" class="form-control"
                                    name="RENT_PER_SQ_FT" value=" <?php echo $key['RENT_PER_SQ_FT']; ?>" >
                                </div>
                                </div>
                            </div>
                        </div>
                            

                            <div class="col-md-8">
                                <label for="comment"><b>Description</b></label>
                                <textarea class="form-control" style="border-radius:15px; "rows="8" id="comment" name="property_description"><?php echo $key['property_discription'];?></textarea>
    </div>
                        


                       
                       
                            <div class="col-md-4">
                                <div class="col-md-12">
                                <div class="form-group">
                                    <label style="margin-left: 20px;"><strong>Total Rent</strong></label>
                                    <input type="text" class="form-control" name="RENT_PER_SQ_FT"  value=" <?php echo $key['total_rent']; ?>">
                                </div>  
                            </div>
                          

                    
                </div>
                    </div>

            <div class="col-md-12">
                <button type="submit" class="btn btn-success" style="float: right;border-radius: 25px;">Submit</button> 
            </div>
</div>
             
 </div>
                   
</form>
<?php } ?>

            </div>
        </div>


        <!-- #END# Advanced Select --> 
        <!-- Input Slider -->
       
</section>
<!-- Jquery Core Js --> 
<script src="<?php echo base_url();?>assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="<?php echo base_url();?>assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

<script src="<?php echo base_url();?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> <!-- Bootstrap Colorpicker Js --> 
<script src="<?php echo base_url();?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> <!-- Input Mask Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/multi-select/js/jquery.multi-select.js"></script> <!-- Multi Select Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/jquery-spinner/js/jquery.spinner.js"></script> <!-- Jquery Spinner Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script> <!-- Bootstrap Tags Input Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/nouislider/nouislider.js"></script> <!-- noUISlider Plugin Js --> 

<script src="<?php echo base_url();?>assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
<script src="<?php echo base_url();?>assets/js/pages/forms/advanced-form-elements.js"></script> 
</body>
<?php $this->load->view('Admin/include/footer'); ?>
</html>