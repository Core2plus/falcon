<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>:: Falcon Mall ::</title>
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Favicon-->
<link  rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Morris Chart Css-->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/morrisjs/morris.css" />
<!-- Colorpicker Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
<!-- Multi Select Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/multi-select/css/multi-select.css">
<!-- Bootstrap Spinner Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/jquery-spinner/css/bootstrap-spinner.css">
<!-- Bootstrap Tagsinput Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<!-- Bootstrap Select Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.css" />
<!-- noUISlider Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/nouislider/nouislider.min.css" />
<!-- Custom Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="<?php echo base_url();?>assets/css/color_skins.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body class="theme-blue">

<!--header-->
<?php $this->load->view('Admin/include/header_top'); ?>

<!-- Top Bar -->
<?php $this->load->view('Admin/include/header_top'); ?>

<!-- Left Sidebar -->
<?php $this->load->view('Admin/include/nav'); ?>

<!-- Right Sidebar -->
<?php $this->load->view('Admin/include/nav1'); ?>


<!--after top bar-->
<br>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2><strong>Welcome To FALCON</strong></h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                  

                <ul class="breadcrumb float-md-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url('Sell/index'); ?>"><i class="zmdi zmdi-home"></i> Falcon</a></li>
                <li class="breadcrumb-item active"><i class="zmdi zmdi-apps"></i> Booking</li>
                </ul>   

            </div>
        </div>
    </div>
  
        <!-- Advanced Select -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2 style="text-align: center; font-size: 20px;"><strong>PROPERTIES LIST FOR BOOKING</strong></h2>
                        
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4">
                               
                                <select id="categories"  onchange="title_change()" class="form-control form-control-md font-weight-normal">
                                  <option value="1">Select Categories</option>
                                  <?php foreach ($categories->result_array() as $key) {?>
                                  # code...
                                  ?>
                                  <option value="<?php echo $key['category'];?>"><?php echo $key['category'];?></option>
                                  <?php }?>
                                </select>

                            </div>
                           
                            <div class="col-lg-4 col-md-6">
                            <select class="form-control form-control-md font-weight-normal" onchange="title_change()" id="floor">
                               <option value="10101">Select Floor</option>
                               <option value="Ground Floor">Ground Floor</option>
                               <option value="1st Floor">1st Floor</option>
                               <option value="2nd Floor">2nd Floor</option>
                               <option value="3rd Floor">3rd Floor</option>
                            </select>
                            </div>
                        
                            <div class="col-lg-4 col-md-6"> 
                                <select id="title_tb"  onchange="title_change()" class="form-control form-control-md font-weight-normal">
                                  <option value="3">All Property</option>
                                  <option value="1">Booked</option>
                                  <option value="0">Available</option>
                                </select>
                            </div>
                        </div><br><br>
                         <div id="sub_title">
              <table class="table table-hover">
        <tbody>
        <thead>
          <tr>
            <th style="width:5%; font-weight: bold;">No</th>
            <th style="width:10%;font-weight: bold;">Outlet Number</th>
            <th style="width:9%; font-weight: bold;">Floor</th>
            <th style="width:12%;font-weight: bold;">Categories</th>
            <th style="width:11%;font-weight: bold;">Brand</th>
            <th style="width:10%; font-weight: bold;">Area SQ FT</th>
            <th style="width:10%;font-weight: bold;">Rent per SQ FT</th>
            <th style="width:11%;font-weight: bold;">Book</th>
            <th style="width:10%;font-weight: bold;">Action</th>
          </tr>
        </thead>

        <?php
        $no=1;
        foreach ($property->result() as $property ) {
          # code...
        
      ?>
      <tr>
        <td><?php echo $no++;?></td>
        <td><?php echo $property->Outlet_Number;?></td>
        <td><?php echo $property->property_floor;?></td>
        <td> <?php echo $property->Categories;?></td>
        <td><?php echo $property->brand_name;?></td>
        <td><?php echo $property->AREA_SQ_FT;?></td>
        <td><?php echo $property->RENT_PER_SQ_FT;?></td>
        <td><?php if($property->property_book==0){?>
        <p class="btn btn-success" style="color:white; ">Available</p>
        <?php } else{ ?>
        <p class="btn btn-danger" style="color:white;">Booked</p>
        <?php
        }
        ?>
        </td>
        <td>
         <div class="row">
            <div class="col-md-5">

          <a href="<?php echo base_url('Sell/viewbooking/').$property->property_id; ?>">
          <button type="button" value="<?php echo $property->property_id;?>" class="btn btn-default"><i class="fa fa-eye"></i></button>
           </div>
           <div class="col-md-5">

          <?php if($property->property_book!=0){?>
        <a href="<?php echo base_url('Sell/showbookingedit/').$property->property_id; ?>">
        <button type="button" value="<?php echo $property->property_id;?>" class="btn btn-primary"><i class="fa fa-edit"></i></button>
      </a>

         <!-- <a href="<?php echo base_url('Sell/property_delete/').$property->property_id; ?>">-->
        <?php } else{ ?>
           <a href="<?php echo base_url('Sell/booking/').$property->property_id; ?>">
        <button type="button" value="<?php echo $property->property_id;?>" class="btn btn-success"><i class="fa fa-clone"></i></button></a>
      
          <?php
          }
          ?>
          </div>
          </div>
        </td>
 <?php }?>
    <?php
       foreach ($available->result() as $property ) {
          # code...
        
      ?>
      <tr>
        <td><?php echo $no++;?></td>
        <td><?php echo $property->Outlet_Number;?></td>
        <td><?php echo $property->property_floor;?></td>
        <td> <?php echo $property->Categories;?></td>
     <td></td>

 
        <td><?php echo $property->AREA_SQ_FT;?></td>

        <td><?php echo $property->RENT_PER_SQ_FT;?></td>
       
        <td><?php if($property->property_book==0){?>
          <p class="btn btn-success" style="color:white; ">Available</p>
          <?php } else{ ?>
            <p class="btn btn-danger" style="color:white;">Booked</p>
            <?php
          }
          ?>
        </td>
        <td>
          <div class="row">
            <div class="col-md-5">

          <button type="button" value="<?php echo $property->property_id;?>" class="btn btn-default"><i class="fa fa-eye"></i></button>
           </div>
           <div class="col-md-5">

          <?php if($property->property_book!=0){?>
        <a href="<?php echo base_url('sell/showbookingedit/').$property->property_id; ?>">
        <button type="button" value="<?php echo $property->property_id;?>" class="btn btn-primary"><i class="fa fa-edit"></i></button>
      </a>

          <!--<a href="<?php echo base_url('sell/property_delete/').$property->property_id; ?>">-->
        <?php } else{ ?>
           <a href="<?php echo base_url('sell/booking/').$property->property_id; ?>">
        <button type="button" value="<?php echo $property->property_id;?>" class="btn btn-success"><i class="fa fa-clone"></i></button></a>
      
          <?php
          }
          ?>
          </div>
          </div>
        </td>

</tr>

        
      </tr>
    </tbody>

                    </div>
                </div>
            <?php }?>
            </div>
        </div>


        <!-- #END# Advanced Select --> 
        <!-- Input Slider -->
       
</section>

<!-- Jquery Core Js --> 
<script src="<?php echo base_url();?>assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="<?php echo base_url();?>assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

<script src="<?php echo base_url();?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> <!-- Bootstrap Colorpicker Js --> 
<script src="<?php echo base_url();?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> <!-- Input Mask Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/multi-select/js/jquery.multi-select.js"></script> <!-- Multi Select Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/jquery-spinner/js/jquery.spinner.js"></script> <!-- Jquery Spinner Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script> <!-- Bootstrap Tags Input Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/nouislider/nouislider.js"></script> <!-- noUISlider Plugin Js --> 

<script src="<?php echo base_url();?>assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
<script src="<?php echo base_url();?>assets/js/pages/forms/advanced-form-elements.js"></script> 


<script>
  function title_change()
  {
    var x =document.getElementById('title_tb').value;
    var y =document.getElementById('categories').value;
    var z =document.getElementById('floor').value;
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('GET',"ajax?title="+x +"&categories="+y+"&floor="+z,false);

    xmlHttp.send(null);
  document.getElementById('sub_title').innerHTML=xmlHttp.responseText;
  //alert(xmlHttp.responseText);
  }
</script>
</body>
</html>