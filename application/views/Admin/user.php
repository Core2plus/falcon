
<?php $this->load->view('Admin/include/header'); ?>
<?php $this->load->view('Admin/include/header_top'); ?>

<main class="cd-main-content">
  <?php $this->load->view('Admin/include/nav'); ?>
        
        <div class="content-wrapper" background="<?php echo base_url(); ?>assets/img/bck.jpg">

           <form action="<?php echo base_url('Sell/register_Client'); ?>" method="post" enctype="multipart/form-data">
  <div class="container-fluid">
    <h1>Add Client</h1>
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
    <br>
    <p>Please fill in this form to create an account.</p>
    <hr>

    <label for="name"><b>Name</b></label>
    <input type="text" placeholder="Enter Name" name="name" />
    
    <label for="Cnic"><b>CNIC</b></label>
    <input type="text" placeholder="Enter CNIC" name="cnic" />
    

    <label for="number"><b>Contact No</b></label>
    <input type="text" class="form-control" placeholder="Enter Number" name="number" />
    <br>
    <div class="form-group">
      <label for="comment">About Us:</label>
      <textarea class="form-control" name="area" rows="5" id="comment"></textarea>
    </div>
    
    <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Enter Email" name="email" />

    <div class="btn btn-default image-preview-input">
                        
                        <input type="file" accept="image/png, image/jpeg, image/gif" name="image" id="image" /> <!-- rename it -->
                    </div>
                </span>
    <hr>
    
    <input type="submit" class="registerbtn" value="Register" />
  </div>
  
</form>
        </div> <!-- .content-wrapper -->
    </main> <!-- .cd-main-content -->

<?php $this->load->view('Admin/include/footer'); ?>