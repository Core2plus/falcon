<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>:: Falcon Mall ::</title>
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Favicon-->
<link  rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
<!-- Morris Chart Css-->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/morrisjs/morris.css" />
<!-- Colorpicker Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
<!-- Multi Select Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/multi-select/css/multi-select.css">
<!-- Bootstrap Spinner Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/jquery-spinner/css/bootstrap-spinner.css">
<!-- Bootstrap Tagsinput Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<!-- Bootstrap Select Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.css" />
<!-- noUISlider Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/nouislider/nouislider.min.css" />
<!-- Custom Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/color_skins.css">
</head>
<body class="theme-blue">

<!--header-->
<?php $this->load->view('Admin/include/header_top'); ?>

<!-- Top Bar -->
<?php $this->load->view('Admin/include/header_top'); ?>

<!-- Left Sidebar -->
<?php $this->load->view('Admin/include/nav'); ?>

<!-- Right Sidebar -->
<?php $this->load->view('Admin/include/nav1'); ?>


<br>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2><strong>Welcome To FALCON</strong></h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Sell/index'); ?>"><i class="zmdi zmdi-home"></i> Falcon</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Sell/all_booking'); ?>"><i class="zmdi zmdi-apps"></i> Booking</a></li>
                    <li class="breadcrumb-item active">Add Booking</li>
                </ul>
            </div>  
            </div>
        </div>
    </div>
  
        <form action="<?php echo base_url('Sell/bookinginsert'); ?>" method="post" enctype="multipart/form-data">
  <div class="container-fluid">
  <input type="hidden" value="" name="booking_status">
    
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
    <h3 style="font-weight: bold; ">Booking</h3>

 <?php foreach ($property->result_array() as $property_data): ?>
  
 



    <hr>
    <input type="hidden"  value="<?php echo $property_data['property_id']; ?>" placeholder="Property Name" name="property_id" />
    <div class="row">
       <div class="col-md-12">
    <label ><b>Property Details</b></label>
    <hr>

  </div>
    <div class="col-md-4">
    <label ><b>Shop Title</b></label>
    <input  type="text" class="form-control"  value="<?php echo $property_data['Categories']; ?>" disabled name="shoptitle" style="border-radius:25px;"/>
  </div>
  <div class="col-md-4">
    <label> <b>Shop Number</b></label>
    <input  type="text" class="form-control" placeholder="Property Number" disabled value="<?php echo $property_data['Outlet_Number']; ?>"  name="shopno" style="border-radius:25px;"/>
  </div>
  <div class="col-md-4">
    <label  for="floor"><b>Shop Floor</b></label>
    <input type="text" class="form-control"  placeholder="Shop Floor" disabled   value="<?php echo $property_data['property_floor']; ?>" name="Floor" style="border-radius:25px;" /> 
  </div>
</div>
  <div class="row">
    
    <input type="hidden"  class="form-control" placeholder="Area SQ FT"  value="<?php echo $property_data['AREA_SQ_FT']; ?>" name="AREA_SQ_FT" style="border-radius:25px;"/>
    <div class="col-md-4">

    <label  ><b> Area SQ FT </b></label>
    <input type="text"  class="form-control" placeholder="Area SQ FT" disabled  value="<?php echo $property_data['AREA_SQ_FT']; ?>" name="AREA_SQ_FT" style="border-radius:25px;"/>
</div>
<div class="col-md-4">

    <label  ><b> Rent per SQ FT </b></label>
    <input type="text" class="form-control"  placeholder="Rent per SQ FT" disabled  value="<?php echo $property_data['RENT_PER_SQ_FT']; ?>" name="RENT_PER_SQ_FT" style="border-radius:25px;"/>
</div>
<div class="col-md-4">
    <label  ><b> Total Rent</b></label>
    <input type="text" class="form-control"  placeholder="Total Rent" disabled  value="<?php echo $property_data['total_rent']; ?>" name="RENT_PER_SQ_FT" style="border-radius:25px;"/>
  </div>
  </div>
     <?php endforeach; ?>
    <div class="row">

  <div class="col-md-12">
    <hr>
    <label ><b>Client Details</b></label>
    <hr>
  </div>
</div>
  <div class="row">
<div class="col-md-12">

    <label ><b>Brand Name</b></label>
    <select  class="form-control" onchange="representative()" required="required" id="brand_id"  name="brand_id" style="border-radius:25px;">
      <option>Select brand</option>
      <?php
       foreach ($company_name as $key) {
         # code...
       ?>
      <option value="<?php echo $key->brand_id; ?>"><?php echo $key->company_name; ?>&nbsp;<?php echo $key->brand_name; ?> </option>
      <?php }
      ?>

    </select>  
</div>
<div class="col-md-6" id="representative">
    
  
</div>
</div>
<br>
<div class="row" id="client_data">


</div>

    <div class="row">

  <div class="col-md-12">
    <hr>
    <label ><b>Booking Form</b></label>
    <hr>
  </div>
    <div class="col-md-4">
      <label ><b>Book Date</b></label>
      <input type="date" class="form-control" placeholder="Booking Date" required="required" name="bookdate" style="border-radius:25px;" />
    </div>
    <div class="col-md-4">
      <label ><b>Booking Price</b></label>
      <input type="text" class="form-control" placeholder="Booking Book" required="required" name="bookingprice" style="border-radius:25px;"/>
    </div>
    <div class="col-md-4">
      <label ><b>Rental Per SQ FT</b></label>
      <input type="text" class="form-control" placeholder="Rental Per SQ FT" required="required" name="rent_SQ_FT" style="border-radius:25px;" /> 
    </div>
    
    </div>
    
    <div class="row" >
      
      <div class="col-md-2">
      <label ><b>Total Rent</b></label>
    
      <input type="text" class="form-control" placeholder="Total Rent" disabled="disabled" style="border-radius:25px;"/> 
    </div>
        <div class="row">


              <label ><b>Rental Date</b></label>
          

          <div class="col-md-3" style="margin-top:5%;">
            <select class="form-control" style="border-radius:25px;" name='rent_date' required="required" id='dayddl'>
                <option value='1'>1</option>
                <option value='2'>2</option>
                <option value='3'>3</option>
                <option value='4'>4</option>
                <option value='5'>5</option>
                <option value='6'>6</option>
                <option value='7'>7</option>
                <option value='8'>8</option>
                <option value='9'>9</option>
                <option value='10'>10</option>
                <option value='11'>11</option>
                <option value='12'>12</option>
                <option value='13'>13</option>
                <option value='14'>14</option>
                <option value='15'>15</option>
                <option value='16'>16</option>
                <option value='17'>17</option>
                <option value='18'>18</option>
                <option value='19'>19</option>
                <option value='20'>20</option>
                <option value='21'>21</option>
                <option value='22'>22</option>
                <option value='23'>23</option>
                <option value='24'>24</option>
                <option value='25'>25</option>
                <option value='26'>26</option>
                <option value='27'>27</option>
                <option value='28'>28</option>
                <option value='29'>29</option>
                <option value='30'>30</option>
                <option value='31'>31</option>
            </select>
          </div>
          <div class="col-md-3" style="margin-top:5%;">
           
            <select class="form-control" style="border-radius:25px;" name="rent_month" required="required" id='gMonth1'>
                <option value=''>--Select Month--</option>
                <option selected value='1'>Janaury</option>
                <option value='2'>February</option>
                <option value='3'>March</option>
                <option value='4'>April</option>
                <option value='5'>May</option>
                <option value='6'>June</option>
                <option value='7'>July</option>
                <option value='8'>August</option>
                <option value='9'>September</option>
                <option value='10'>October</option>
                <option value='11'>November</option>
                <option value='12'>December</option>
            </select> 
          </div> 
          
           <div class="col-md-3" style="margin-top:5%;">
            <select class="form-control" style="border-radius:25px;" id="year" name="rent_year" required="required">
                <option value='1'>2017</option>
                <option value='2'>2018</option>   
            </select>
          </div>
      
    </div>
 </div>
    
    <div class="row">
     <strong><label for="comment" >Description</label></strong> 
      <textarea class="form-control" name="pdesc"  rows="5" id="comment"  style="border-radius:25px;"> </textarea>
    </div>
     <div class="file btn btn-lg btn-primary" style="  position: relative;
     overflow: hidden; font-size:15px; border-radius: 10px;"> <i class="fa fa-folder"></i>              
                            Upload
    <input type="file" style=" position: absolute;  font-size: 50px;  opacity: 0;
  right: 0;  top: 0;" name="imgName"/>
                        </div>
                      

<hr>
    
    <input type="submit" class="btn btn-primary" value="Submit" style="border-radius:10px; padding-left:40px; padding-right:40px; float:right;" />
      </div>
      
</form> <!-- Advanced Select -->
        <div class="row clearfix">
          


        <!-- #END# Advanced Select --> 
        <!-- Input Slider -->
       
</section>
<!-- Jquery Core Js --> 
<script src="<?php echo base_url();?>assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="<?php echo base_url();?>assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

<script src="<?php echo base_url();?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> <!-- Bootstrap Colorpicker Js --> 
<script src="<?php echo base_url();?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> <!-- Input Mask Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/multi-select/js/jquery.multi-select.js"></script> <!-- Multi Select Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/jquery-spinner/js/jquery.spinner.js"></script> <!-- Jquery Spinner Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script> <!-- Bootstrap Tags Input Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/nouislider/nouislider.js"></script> <!-- noUISlider Plugin Js --> 

<script src="<?php echo base_url();?>assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
<script src="<?php echo base_url();?>assets/js/pages/forms/advanced-form-elements.js"></script> 
</body>
<?php $this->load->view('Admin/include/footer'); ?>
</html>