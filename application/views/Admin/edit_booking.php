<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>:: Falcon Mall ::</title>
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Favicon-->
<link  rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
<!-- Morris Chart Css-->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/morrisjs/morris.css" />
<!-- Colorpicker Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
<!-- Multi Select Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/multi-select/css/multi-select.css">
<!-- Bootstrap Spinner Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/jquery-spinner/css/bootstrap-spinner.css">
<!-- Bootstrap Tagsinput Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<!-- Bootstrap Select Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.css" />
<!-- noUISlider Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/nouislider/nouislider.min.css" />
<!-- Custom Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/color_skins.css">
</head>
<body class="theme-blue">

<!--header-->
<?php $this->load->view('Admin/include/header_top'); ?>

<!-- Top Bar -->
<?php $this->load->view('Admin/include/header_top'); ?>

<!-- Left Sidebar -->
<?php $this->load->view('Admin/include/nav'); ?>

<!-- Right Sidebar -->
<?php $this->load->view('Admin/include/nav1'); ?>


<br>
<section class="content">
        <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2><strong>Welcome To FALCON</strong></h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Sell/index'); ?>"><i class="zmdi zmdi-home"></i> Falcon</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url('Sell/all_booking'); ?>"><i class="zmdi zmdi-apps"></i> Booking</a></li>
                    <li class="breadcrumb-item active">Edit Booking</li>
                </ul>
            </div>        

            </div>
        </div>
          <?php foreach ($booking->result_array() as $key2 ) {
          # code...
        ?>
      
           <form action="<?php echo base_url('Sell/bookingedit/').$key2["book_id"]; ?>" method="post" enctype="multipart/form-data">
            <?php 
            }
            ?>
        <!-- Advanced Select -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>EDIT BOOKING</strong> </h2>                           
                    </div>
                     <?php foreach ($property->result_array() as $key1 ) {
          # code...
        ?>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-md-4">
                                    <label><strong>Outlet Number</strong></label>
                                    <input type="text" class="form-control" disabled="disabled" value="<?php echo $key1['Outlet_Number'];?>">
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><strong>Shop Floor</strong></label>
                                    <input type="text" class="form-control" disabled="disabled" value="<?php echo $key1['property_floor'];?>" name="bookingprice">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><strong>Category</strong></label>
                                    <input type="text" class="form-control" disabled="disabled" value="<?php echo $key1['Categories'];?>"  name="RENT_PER_SQ_FT" >
                                </div>
                            </div>
 <?php
    }
    ?>
  
    <label><b>Booking Details</b></label>
    <hr>
  <?php foreach ($booking->result_array() as $key ) {
          # code...
        ?>
        <div class="row" style="margin-right: 50%;">
                                <div class="col-md-5"><br>
                                    <label><strong>Book Date</strong></label>
                                    <input type="date" class="form-control" value="<?php echo $key['bookingdate'];?>" name="bookdate">
                                </div>
                        
                            <div class="col-md-3">
                                <div class="form-group"><br>
                                    <label><strong>Booking Price</strong></label>
                                    <input type="text" class="form-control" value="<?php echo $key['bookingprice'];?>" name="bookingprice">
                                </div>
                        </div>
                            <div class="col-md-4">
                                <div class="form-group"><br>
                                    <label><strong>Rent per SQ FT</strong></label>
                                    <input type="text" class="form-control" value="<?php echo $key['RENT_PER_SQ_FT'];?>" name="rent_SQ_FT">
                                </div>
                            </div>
                </div>
            
            <label ><b>Rental Date</b></label>
            <div class="col-md-3" style="margin-left: -7%;"><br>
            <select class="form-control show-tick" name='rent_date' required="required" id='dayddl'>
             <option value='<?php echo $key['rent_date'];?>'> <?php echo $key['rent_date'];?></option>     
                <option value='1'>1</option>
                <option value='2'>2</option>
                <option value='3'>3</option>
                <option value='4'>4</option>
                <option value='5'>5</option>
                <option value='6'>6</option>
                <option value='7'>7</option>
                <option value='8'>8</option>
                <option value='9'>9</option>
                <option value='10'>10</option>
                <option value='11'>11</option>
                <option value='12'>12</option>
                <option value='13'>13</option>
                <option value='14'>14</option>
                <option value='15'>15</option>
                <option value='16'>16</option>
                <option value='17'>17</option>
                <option value='18'>18</option>
                <option value='19'>19</option>
                <option value='20'>20</option>
                <option value='21'>21</option>
                <option value='22'>22</option>
                <option value='23'>23</option>
                <option value='24'>24</option>
                <option value='25'>25</option>
                <option value='26'>26</option>
                <option value='27'>27</option>
                <option value='28'>28</option>
                <option value='29'>29</option>
                <option value='30'>30</option>
                <option value='31'>31</option>
            </select>
          </div>
                           
            
            <div class="col-md-4"><br>
                <select class="form-control show-tick" name="rent_month" required="required" id='gMonth1'>
                <option selected value='<?php echo $key['rent_month'];?>'> <?php echo $key['rent_month'];?></option>
                <option  value='Janaury'>Janaury</option>
                <option value='February'>February</option>
                <option value='March'>March</option>
                <option value='April'>April</option>
                <option value='May'>May</option>
                <option value='June'>June</option>
                <option value='July'>July</option>
                <option value='August'>August</option>
                <option value='September'>September</option>
                <option value='October'>October</option>
                <option value='November'>November</option>
                <option value='December'>December</option>
                </select>
            </div>
                        
            <div class="col-md-4"><br>
                <select class="form-control" style="border-radius:25px;" id="year" name="rent_year" required="required"> 
                      <option selected value='<?php echo $key['rent_year'];?>'> <?php echo $key['rent_year'];?></option>
                </select>
            </div>

            <div class="col-md-12"><br>
                    <label for="comment"><b>Description</b></label>
                    <textarea class="form-control" style="border-radius:15px; "rows="5" name="pdesc" id="comment" placeholder="Enter Description Here..." ><?php echo $key['description'];?></textarea>
           </div>
                        
    </div>
    <br><br>

        <div class="file btn btn-lg btn-primary" style="  position: relative;
            overflow: hidden;font-size:15px; border-radius: 10px;"> <i class="fa fa-folder"></i>              
                                Upload
            <input type="file" style=" position: absolute;  font-size: 50px;  opacity: 0;
            right: 0;  top: 0;" name="imgName"/>
        </div>

        <input type="submit" class="btn btn-primary" value="Update" style="border-radius:10px; padding-left:40px; padding-right:40px; float:right;"/>
           
                </div>
            </div>
        </div>
</div>


        <!-- #END# Advanced Select --> 
        <!-- Input Slider -->
       </form>
</section>
<?php }?>
        </div> <!-- .content-wrapper -->
    </main> <!-- .cd-main-content -->

<?php $this->load->view('Admin/include/footer'); ?>
<script >
var start = 2017;
var end = new Date().getFullYear();
var options = "";
for(var year = start ; year <=end; year++){
  options += "<option>"+ year +"</option>";
}
document.getElementById("year").innerHTML = options;
</script>
<!-- Jquery Core Js --> 
<script src="<?php echo base_url();?>assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="<?php echo base_url();?>assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

<script src="<?php echo base_url();?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> <!-- Bootstrap Colorpicker Js --> 
<script src="<?php echo base_url();?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> <!-- Input Mask Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/multi-select/js/jquery.multi-select.js"></script> <!-- Multi Select Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/jquery-spinner/js/jquery.spinner.js"></script> <!-- Jquery Spinner Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script> <!-- Bootstrap Tags Input Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/nouislider/nouislider.js"></script> <!-- noUISlider Plugin Js --> 

<script src="<?php echo base_url();?>assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
<script src="<?php echo base_url();?>assets/js/pages/forms/advanced-form-elements.js"></script> 
</body>
<?php $this->load->view('Admin/include/footer'); ?>
</html>