<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>:: Falcon Mall ::</title>
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Favicon-->
<link  rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
<!-- Morris Chart Css-->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/morrisjs/morris.css" />
<!-- Colorpicker Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
<!-- Multi Select Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/multi-select/css/multi-select.css">
<!-- Bootstrap Spinner Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/jquery-spinner/css/bootstrap-spinner.css">
<!-- Bootstrap Tagsinput Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<!-- Bootstrap Select Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.css" />
<!-- noUISlider Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/nouislider/nouislider.min.css" />
<!-- Custom Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">


<link rel="stylesheet" href="<?php echo base_url();?>assets/css/color_skins.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body class="theme-blue">

<!--header-->
<?php $this->load->view('Admin/include/header_top'); ?>

<!-- Top Bar -->
<?php $this->load->view('Admin/include/header_top'); ?>

<!-- Left Sidebar -->
<?php $this->load->view('Admin/include/nav'); ?>

<!-- Right Sidebar -->
<?php $this->load->view('Admin/include/nav1'); ?>


<!--after top bar-->
<br>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2><strong>Welcome To FALCON</strong></h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                 <a href="<?php echo base_url('sell/show_lead_insert')?>">
                 <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button></a>

                <ul class="breadcrumb float-md-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url('Sell/index'); ?>"><i class="zmdi zmdi-home"></i> Falcon</a></li>
                <li class="breadcrumb-item"><i class="zmdi zmdi-swap-alt"></i> Lead</li>
                </ul>   

            </div>
        </div>
    </div>

  
        <!-- Advanced Select -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2 style="text-align: center; font-size: 20px;"><strong>LEADS</strong></h2>
                    </div>
                    <div class="body">

              <div id="sub_title">
              <table class="table table-hover">
        <tbody>
        <thead>
          <tr>
            <th style="width:5%; font-weight: bold;">No</th>
            <th style="width:10%;font-weight: bold;">Name</th>
            <th style="width:10%; font-weight: bold;">Phone No</th>
            <th style="width:15%;font-weight: bold;">Email</th>
            <th style="width:30%; font-weight: bold;">Description</th>
            <th style="width:12%;font-weight: bold;">Action</th>
          </tr>
        </thead>
        
        <tbody>
        <?php
        $no=1;
        foreach ($lead->result_array() as $key) {
         # code...
        
      ?>
      <tr>
        <td><?php echo $no++;?></td>
        <td><?php echo $key['lead_name'];?></td>
        <td><?php echo $key['lead_phone'];?></td>
        <td><?php echo  $key['lead_email'];?></td>
        <td><?php echo  $key['description'];?></td>
        <td>

              <a href="<?php echo base_url('sell/show_lead_edit/').$key['lead_id'];?>">
              <button class="btn btn-primary btn-sm" ><i class="zmdi zmdi-edit"></i></button> </a>

              <a href="<?php echo base_url('sell/lead_delete/').$key['lead_id'];?>">
              <button class="btn btn-danger btn-sm"><i class="zmdi zmdi-delete"></i></button> </as>


            </td>


            <?php }?>
    </tbody>

                    </div>
                </div>
            </div>
        </div>


        <!-- #END# Advanced Select --> 
        <!-- Input Slider -->
       
</section>
<!-- Jquery Core Js --> 
<script src="<?php echo base_url();?>assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="<?php echo base_url();?>assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

<script src="<?php echo base_url();?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> <!-- Bootstrap Colorpicker Js --> 
<script src="<?php echo base_url();?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> <!-- Input Mask Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/multi-select/js/jquery.multi-select.js"></script> <!-- Multi Select Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/jquery-spinner/js/jquery.spinner.js"></script> <!-- Jquery Spinner Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script> <!-- Bootstrap Tags Input Plugin Js --> 
<script src="<?php echo base_url();?>assets/plugins/nouislider/nouislider.js"></script> <!-- noUISlider Plugin Js --> 

<script src="<?php echo base_url();?>assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
<script src="<?php echo base_url();?>assets/js/pages/forms/advanced-form-elements.js"></script> 
</body>
</html>