<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>:: Falcon Mall ::</title>
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Favicon-->
<link  rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
<!-- Morris Chart Css-->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/morrisjs/morris.css" />
<!-- Colorpicker Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
<!-- Multi Select Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/multi-select/css/multi-select.css">
<!-- Bootstrap Spinner Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/jquery-spinner/css/bootstrap-spinner.css">
<!-- Bootstrap Tagsinput Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<!-- Bootstrap Select Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.css" />
<!-- noUISlider Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/nouislider/nouislider.min.css" />
<!-- Custom Css -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">


<link rel="stylesheet" href="<?php echo base_url();?>assets/css/color_skins.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body class="theme-blue">

<!--header-->
<?php $this->load->view('Admin/include/header_top'); ?>

<!-- Top Bar -->
<?php $this->load->view('Admin/include/header_top'); ?>

<!-- Left Sidebar -->
<?php $this->load->view('Admin/include/nav'); ?>

<!-- Right Sidebar -->
<?php $this->load->view('Admin/include/nav1'); ?>


<!--after top bar-->
<br>
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2><strong>Welcome To FALCON</strong></h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                    <a href="<?php echo base_url('sell/property')?>">

                 <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button></a>

                <ul class="breadcrumb float-md-right">
                <li class="breadcrumb-item"><a href="<?php echo base_url('Sell/index'); ?>"><i class="zmdi zmdi-home"></i> Falcon</a></li>
                <li class="breadcrumb-item"><i class="zmdi zmdi-shopping-cart"></i> Property</li>
                </ul>   

            </div>
        </div>
    </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-md-12">
                <div class="card">
                    <div class="body">
                        <div class="header">
                           <h2 style="text-align: center; font-size: 20px;"><strong>PROPERTIES LIST</strong></h2>
                       </div>
          <div id="sub_title">
          <table class="table table-hover">
    <tbody>
    <thead>
      <tr>
        <th style="width:1%; font-weight: bold;">No</th>
        <th style="width:8%;font-weight: bold;">Categories</th>
        <th style="width:10%;font-weight: bold;">Outlet Number</th>
        <th style="width:7%; font-weight: bold;">Floor</th> 
        <th style="width:6%; font-weight: bold;">Area SQ FT</th>
        <th style="width:8%;font-weight: bold;">Rent per SQ FT</th>
        <th style="width:5%;font-weight: bold;">Total Rent</th> 
        <th style="width:0.1%;font-weight: bold;">Book</th>
        <th style="width:6%;font-weight: bold;">Action</th>
      </tr>
    </thead>

    <td>
        <tbody>
      <?php
        $no=1;
        foreach ($property->result() as $property ) {
          # code...
        
      ?>
    <tr style = "">
        <td><?php echo $no++;?></td>
        <td><span class="align-middle"><?php echo $property->Outlet_Number;?></span></td>
        <td><?php echo $property->property_floor;?></td>
        <td><?php echo $property->Categories;?></td>
        <td><?php echo $property->AREA_SQ_FT;?></td>
        <td><?php echo $property->RENT_PER_SQ_FT;?></td>
        <td><?php echo $property->total_rent;?></td>
        


        <td>
            <?php if($property->property_book==0){?>
            <p class="btn btn-success " >Available</p>
              <?php } 
            else
                { ?>
                <p class="btn btn-danger" ">Booked</p>
                <?php
                }?>
        </td>
        
        <td> 
            <div class="row" >
              <span  style="margin-left: 10%">
                  <a href="<?php echo base_url('sell/showeditproperty/').$property->property_id; ?>">
                      <button class="btn btn-primary btn-sm " value="<?php echo $property->property_id;?>">
                      <i class="zmdi zmdi-edit"></i></button>
                  </a>
                    
                  <a href="<?php echo base_url('sell/property_delete/').$property->property_id; ?>">
                      <button class="btn btn-danger btn-sm" value="<?php echo $property->property_id;?>">
                      <i class="zmdi zmdi-delete"></i></button>
                  </a>
              </span>
            </div>
        </td>
    </tr>

         <?php }?>
        <?php
       foreach ($available->result() as $property ) {
          # code...
        
      ?>
      <tr>
        <td><?php echo $no++;?></td>
        <td><?php echo $property->Outlet_Number;?></td>
        <td><?php echo $property->property_floor;?></td>
        <td> <?php echo $property->Categories;?></td>
     <td></td>

 
        <td><?php echo $property->AREA_SQ_FT;?></td>

        <td><?php echo $property->RENT_PER_SQ_FT;?></td>
       
        <td><?php if($property->property_book==0){?>
          <p class="btn btn-success" style="color:white; ">Available</p>
          <?php } else{ ?>
            <p class="btn btn-danger" style="color:white;">Booked</p>
            <?php
          }
          ?>
        </td>
        <td> 
            <div class="row" >
              <span  style="margin-left: 10%">
                  <a href="<?php echo base_url('sell/showeditproperty/').$property->property_id; ?>">
                      <button class="btn btn-primary btn-sm " value="<?php echo $property->property_id;?>">
                      <i class="zmdi zmdi-edit"></i></button>
                  </a>
                    
                  <a href="<?php echo base_url('sell/property_delete/').$property->property_id; ?>">
                      <button class="btn btn-danger btn-sm" value="<?php echo $property->property_id;?>">
                      <i class="zmdi zmdi-delete"></i></button>
                  </a>
              </span>
            </div>
        </td>
        
          <?php
          }
          ?>





</tbody>
                    </div>
                </div>
            </div>
        </div>
    </div>










</section>
<!-- Jquery Core Js --> 
<script src="<?php echo base_url();?>assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="<?php echo base_url();?>assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

<script src="<?php echo base_url();?>assets/bundles/mainscripts.bundle.js"></script>
</body>
</html>